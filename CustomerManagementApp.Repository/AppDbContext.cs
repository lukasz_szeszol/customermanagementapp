﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CustomerManagementApp.Domain.Model;

namespace CustomerManagementApp.Repository
{
    public class AppDbContext : DbContext
    {
        public AppDbContext():base("CustomerContext")
        {
                
        }
        public AppDbContext(string nameOrConnectionString) : base(nameOrConnectionString)
        {
        }
        public DbSet<Customer> Customers { get; set; }
    }
}
