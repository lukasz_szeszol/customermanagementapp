﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Threading.Tasks;
using CustomerManagementApp.Domain.Model;

namespace CustomerManagementApp.Repository.Repository
{
    public class CustomerRepository:ICustomerRepository
    {
        private readonly AppDbContext _dbContext;

        public CustomerRepository(AppDbContext dbContext)
        {
            _dbContext = dbContext;

        }

        public async Task<Customer> GetAsync(int id)
        {
            return 
                await _dbContext
                .Customers
                .FindAsync(id);
        }

        public async Task<IEnumerable<Customer>> GetAllAsync()
        {
            return 
                await _dbContext
                .Customers
                .AsNoTracking()
                .ToListAsync();
        }

        public void Add(Customer customer)
        {
            if (customer == null)
            {
                throw new ArgumentNullException(nameof(customer));
                
            }
            _dbContext
                .Customers
                .Add(customer);
        }

        public void Update(Customer customer)
        {
            if (customer == null)
            {
                throw new ArgumentNullException(nameof(customer));
            }
            _dbContext.Customers.Attach(customer);
            _dbContext.Entry(customer).State = EntityState.Modified;
        }

        public void Remove(Customer customer)
        {
            if (customer == null)
            {
                throw new ArgumentNullException(nameof(customer));
            }
            _dbContext
                .Customers
                .Remove(customer);
        }

        public async Task<int> SaveChangesAsync()
        {
            return 
                await _dbContext
                .SaveChangesAsync();
        }
    }
}
