﻿using System.Collections.Generic;
using System.Threading.Tasks;
using CustomerManagementApp.Domain.Model;

namespace CustomerManagementApp.Repository.Repository
{
    public interface ICustomerRepository :IRepository
    {
        Task<Customer> GetAsync(int id);
        Task<IEnumerable<Customer>> GetAllAsync();
        void Add(Customer customer);
        void Update(Customer customer);
        void Remove(Customer customer);
        Task<int> SaveChangesAsync();

    }
}
