﻿using Autofac;
using CustomerManagementApp.Infrastructure.Settings;
using CustomerManagementApp.Repository;
using CustomerManagementApp.Repository.Repository;

namespace CustomerManagementApp.Infrastructure.IoC.Modules
{
    public class RepositoryModule: Module
    {
        private readonly IConfiguration _configuration;
        public RepositoryModule(IConfiguration configuration)
        {
            _configuration = configuration;
        }
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<CustomerRepository>().As<ICustomerRepository>().InstancePerRequest();
            builder.Register(c => new AppDbContext(_configuration.ConnectionString));        }
    }
}
