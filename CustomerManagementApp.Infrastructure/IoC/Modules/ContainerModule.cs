﻿using Autofac;
using CustomerManagementApp.Infrastructure.Settings;

namespace CustomerManagementApp.Infrastructure.IoC.Modules
{
    public class ContainerModule: Autofac.Module
    {
        private readonly IConfiguration _configuration;
        public ContainerModule(IConfiguration configuration)
        {
            _configuration = configuration;
        }
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterModule(new RepositoryModule(_configuration));
            builder.RegisterModule<ServiceModule>();
        }
    }
}
