﻿using Autofac;
using CustomerManagementApp.Service.Service;

namespace CustomerManagementApp.Infrastructure.IoC.Modules
{
    public class ServiceModule: Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<CustomerService>().As<ICustomerService>().InstancePerRequest();
        }
    }
}
