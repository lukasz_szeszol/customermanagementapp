﻿using CustomerManagementApp.Repository.Repository;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using CustomerManagementApp.Domain.Model;

namespace CustomerManagementApp.Service.Service
{
    public class CustomerService:ICustomerService
    {
        private readonly ICustomerRepository _customerRepository;

        public CustomerService(ICustomerRepository customerRepository)
        {
            _customerRepository = customerRepository;
        }

        public CustomerService()
        {
        }

        public async Task<Customer> GetCustomerAsync(int customerId)
        {
            return await _customerRepository.GetAsync(customerId);
        }

        public async Task<IEnumerable<Customer>> GetAllCustomersAsync()
        {
            return await _customerRepository.GetAllAsync();
        }

        public async Task CreateCustomerAsync(Customer customer)
        {
            if (customer == null)
            {
                throw new ArgumentNullException(nameof(customer));
            }

            _customerRepository.Add(customer);
            await _customerRepository.SaveChangesAsync();
        }

        public async  Task UpdateCustomerAsync(Customer customer)
        {
            if (customer == null)
            {
                throw new ArgumentNullException(nameof(customer));
                
            }

            _customerRepository.Update(customer);
             await _customerRepository.SaveChangesAsync();
        }

        public async Task DeleteCustomerAsync(int customerId)
        {
            var customer = await _customerRepository.GetAsync(customerId);

            if (customer != null)
            {
                _customerRepository.Remove(customer);
                await _customerRepository.SaveChangesAsync();
            }
        }
    }
}
