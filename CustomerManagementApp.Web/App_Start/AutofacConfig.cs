﻿using System.Web.Mvc;
using Autofac;
using CustomerManagementApp.Infrastructure.IoC.Modules;
using CustomerManagementApp.Infrastructure.Settings;
using Autofac.Integration.Mvc;

namespace CustomerManagementApp.Web
{
    public class AutofacConfig
    {
        public static void InitializeAutofac()
        {
            var builder = new ContainerBuilder();

            IConfiguration configuration = new Configuration();
            configuration.ConnectionString =
                System.Configuration.ConfigurationManager.ConnectionStrings["CustomerContext"].ConnectionString;

            builder.RegisterControllers(typeof (MvcApplication).Assembly);

            builder.RegisterModule(new ContainerModule(configuration));

            var container = builder.Build();

            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));
        }
    }
}