﻿$('#createCustomer').on('click', function(e) {

        $('.modal-title').html('Add customer');

        var url = '/Customer/AddCustomerPartial';
        console.log(url);

        $.ajax({
            url: url,
            type: 'GET',
            cache: false,
            success: function(result) {
                $('#modalContent').html(result);
                $('#myModal').modal('show');
            },
            error: function(result) {
                console.log(result);
            },
        });

    });

    $('.editCustomer').on('click', function(e) {

        $('.modal-title').html('Edit customer');

        var url = '/Customer/EditCustomer';
        var buttonId = $(this).attr('id');
        var id = buttonId.substr(buttonId.indexOf('-') + 1);

        $.ajax({
            url: url,
            data: { id: id },
            type: 'GET',
            cache: false,
            success: function(result) {
                $('#modalContent').html(result);
                $('#myModal').modal('show');
            }
        });
    });

    $('.deleteCustomer').on('click', function(e) {

        $('.modal-title').html('Delete customer');

        var url = '/Customer/DeleteCustomer';
        var buttonId = $(this).attr('id');
        var id = buttonId.substr(buttonId.indexOf('-') + 1);

        $.ajax({
            url: url,
            data: { id: id },
            type: 'GET',
            cache: false,
            success: function(result) {
                $('#modalContent').html(result);
                $('#myModal').modal('show');
            }
        });
    });