﻿using System.Threading.Tasks;
using System.Web.Mvc;
using CustomerManagementApp.Domain.Model;
using CustomerManagementApp.Service.Service;
using CustomerManagementApp.Web.ViewModel;

namespace CustomerManagementApp.Web.Controllers
{
    public class CustomerController : Controller
    {
        private readonly ICustomerService _customerService;
        public CustomerController(ICustomerService customerService)
        {
            _customerService = customerService;
        }

        public CustomerController()
        {
                
        }
        [HttpGet]
        public async Task<ActionResult> Index()
        {
            var customers = await _customerService.GetAllCustomersAsync();

            return View(customers);
        }
        [HttpGet]
        public ActionResult AddCustomer()
        {
            Customer model = new Customer();

            return View("AddCustomer", model);
        }
        [HttpGet]
        public ActionResult AddCustomerPartial()
        {
            Customer model = new Customer();

            return PartialView("AddCustomer", model);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> AddCustomer(Customer customer)
        {
            if (!ModelState.IsValid)
            {
                return View("AddCustomer", customer);
            }
            await _customerService.CreateCustomerAsync(customer);

            if (customer.CustomerId > 0)
            {
                return RedirectToAction("Index", "Customer");
            }

            return View("AddCustomer", customer);
        }
        [HttpGet]
        public async Task<ActionResult> EditCustomer(int? id)
        {
            Customer model = new Customer();

            if (id.HasValue && id != 0)
            {
                model = await _customerService.GetCustomerAsync(id.Value);
            }

            return PartialView("EditCustomer", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> EditCustomer(Customer model)
        {
            if (ModelState.IsValid &&  model.CustomerId > 0)
            {
               await _customerService.UpdateCustomerAsync(model);

               return RedirectToAction("Index", "Customer");
            }

            return View("EditCustomer", model);
        }
        [HttpGet]
        public async Task<ActionResult> DeleteCustomer(int? id)
        {
            Customer model = new Customer();

            if (id.HasValue && id != 0)
            {
                model = await _customerService.GetCustomerAsync(id.Value);
            }
            var customer = (RemoveCustomerViewModel) model;

            return PartialView("DeleteCustomer", customer);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteCustomer(RemoveCustomerViewModel model)
        {
            if (ModelState.IsValid)
            {
                if (model.Id != 0 && model.Id.HasValue)
                {
                    await _customerService.DeleteCustomerAsync(model.Id.Value);
                }
            }

            return RedirectToAction("Index", "Customer");

        }
    }
}