﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CustomerManagementApp.Domain.Model;

namespace CustomerManagementApp.Web.ViewModel
{
    public class RemoveCustomerViewModel
    {
        public string FullName { get; set; }
        public int? Id { get; set; }
        public static explicit operator RemoveCustomerViewModel(Customer cust)
        {
            if (cust == null) throw new ArgumentNullException(nameof(cust));

            RemoveCustomerViewModel customerToRemove = new RemoveCustomerViewModel()
            {
                Id = cust.CustomerId,
                FullName = $"{cust.Name} {cust.Surname}",
            };

            System.Console.WriteLine("Conversion occurred.");
            return customerToRemove;
        }
    }
}