﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CustomerManagementApp.Web.ViewModel
{
    public class ModalWindowButtons
    {
        public string SubmitButtonDesc{ get; set; } = "Submit";
        public string CancelButtonDesc { get; set; } = "Cancel";
        public string SubmitButtonStyle { get; set; } = "btn-accept";
        public string CancelButtonStyle { get; set; } = "btn-cancel";
    }
}